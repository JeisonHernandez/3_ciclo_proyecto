/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 
 test
package model;

/**
 *
 * @author jonco
 */
public class EmpleadoModel {
    private int idEmpleado;
    private String nombreEmpleado;
    private int idAutobus;

    public EmpleadoModel() {
    }

    public EmpleadoModel(int idEmpleado, String nombreEmpleado, int idAutobus) {
        this.idEmpleado = idEmpleado;
        this.nombreEmpleado = nombreEmpleado;
        this.idAutobus = idAutobus;
    }

    /**
     * @return the idEmpleado
     */
    public int getIdEmpleado() {
        return idEmpleado;
    }

    /**
     * @return the nombreEmpleado
     */
    public String getNombreEmpleado() {
        return nombreEmpleado;
    }

    /**
     * @param nombreEmpleado the nombreEmpleado to set
     */
    public void setNombreEmpleado(String nombreEmpleado) {
        this.nombreEmpleado = nombreEmpleado;
    }

    /**
     * @return the idAutobus
     */
    public int getIdAutobus() {
        return idAutobus;
    }

    /**
     * @param idAutobus the idAutobus to set
     */
    public void setIdAutobus(int idAutobus) {
        this.idAutobus = idAutobus;
    }
    
    
    
}
