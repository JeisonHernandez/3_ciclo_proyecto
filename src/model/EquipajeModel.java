/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author jonco
 */
public class EquipajeModel {
    private int idEquipaje;
    private int idPasajero;
    private int peso;

    public EquipajeModel() {
    }

    public EquipajeModel(int idEquipaje, int idPasajero, int peso) {
        this.idEquipaje = idEquipaje;
        this.idPasajero = idPasajero;
        this.peso = peso;
    }

    /**
     * @return the idEquipaje
     */
    public int getIdEquipaje() {
        return idEquipaje;
    }

    /**
     * @return the idPasajero
     */
    public int getIdPasajero() {
        return idPasajero;
    }

    /**
     * @param idPasajero the idPasajero to set
     */
    public void setIdPasajero(int idPasajero) {
        this.idPasajero = idPasajero;
    }

    /**
     * @return the peso
     */
    public int getPeso() {
        return peso;
    }

    /**
     * @param peso the peso to set
     */
    public void setPeso(int peso) {
        this.peso = peso;
    }
    
    
    
}
