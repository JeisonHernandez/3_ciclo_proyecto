/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author jonco
 */
public class ViajeModel {
    private int idViaje;
    private String idCiudad_Origen;
    private String idCiudad_Destino;
    private int idAutobus;
    private String fechaViaje;
    private long precio;

    public ViajeModel() {
    }
    
    public ViajeModel(int idViaje, String idCiudad_Origen, String idCiudad_Destino, int idAutobus, String fechaViaje, long precio) {
        this.idViaje = idViaje;
        this.idCiudad_Origen = idCiudad_Origen;
        this.idCiudad_Destino = idCiudad_Destino;
        this.idAutobus = idAutobus;
        this.fechaViaje = fechaViaje;
        this.precio = precio;
    }

    /**
     * @return the idViaje
     */
    public int getIdViaje() {
        return idViaje;
    }

    /**
     * @return the idCiudad_Origen
     */
    public String getIdCiudad_Origen() {
        return idCiudad_Origen;
    }

    /**
     * @param idCiudad_Origen the idCiudad_Origen to set
     */
    public void setIdCiudad_Origen(String idCiudad_Origen) {
        this.idCiudad_Origen = idCiudad_Origen;
    }

    /**
     * @return the idCiudad_Destino
     */
    public String getIdCiudad_Destino() {
        return idCiudad_Destino;
    }

    /**
     * @param idCiudad_Destino the idCiudad_Destino to set
     */
    public void setIdCiudad_Destino(String idCiudad_Destino) {
        this.idCiudad_Destino = idCiudad_Destino;
    }

    /**
     * @return the idAutobus
     */
    public int getIdAutobus() {
        return idAutobus;
    }

    /**
     * @param idAutobus the idAutobus to set
     */
    public void setIdAutobus(int idAutobus) {
        this.idAutobus = idAutobus;
    }

    /**
     * @return the fechaViaje
     */
    public String getFechaViaje() {
        return fechaViaje;
    }

    /**
     * @param fechaViaje the fechaViaje to set
     */
    public void setFechaViaje(String fechaViaje) {
        this.fechaViaje = fechaViaje;
    }

    /**
     * @return the precio
     */
    public long getPrecio() {
        return precio;
    }

    /**
     * @param precio the precio to set
     */
    public void setPrecio(long precio) {
        this.precio = precio;
    }
    
    
    
    
    
}
