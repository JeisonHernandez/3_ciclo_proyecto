/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author jonco
 */
public class CiudadModel {
    private String idCiudad;
    private String nombreciudad;

    public CiudadModel() {
    }

    public CiudadModel(String idCiudad, String nombreciudad) {
        this.idCiudad = idCiudad;
        this.nombreciudad = nombreciudad;
    }

    /**
     * @return the idCiudad
     */
    public String getIdCiudad() {
        return idCiudad;
    }

    /**
     * @param idCiudad the idCiudad to set
     */
    public void setIdCiudad(String idCiudad) {
        this.idCiudad = idCiudad;
    }

    /**
     * @return the nombreciudad
     */
    public String getNombreCiudad() {
        return nombreciudad;
    }

    /**
     * @param nombreciudad the nombreciudad to set
     */
    public void setNombreciudad(String nombreciudad) {
        this.nombreciudad = nombreciudad;
    }
    
    public Object[] toArray(){
        Object[] data = {idCiudad, nombreciudad};
        return data;
    }
    
    
}
