/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author jonco
 */
public class PasajeroModel {
    
    private int idPasajero;
    private String nombrePasajero;

    public PasajeroModel() {
    }

    public PasajeroModel(int idPasajero, String nombrePasajero) {
        this.idPasajero = idPasajero;
        this.nombrePasajero = nombrePasajero;
    }
    
    public PasajeroModel(String nombrePasajero) {
        this.nombrePasajero = nombrePasajero;
    }

    /**
     * @return the idPasajero
     */
    public int getIdPasajero() {
        return idPasajero;
    }

    public void setIdPasajero(int idPasajero) {
        this.idPasajero = idPasajero;
    }
    
    

    /**
     * @return the nombrePasajero
     */
    public String getNombrePasajero() {
        return nombrePasajero;
    }

    /**
     * @param nombrePasajero the nombrePasajero to set
     */
    public void setNombrePasajero(String nombrePasajero) {
        this.nombrePasajero = nombrePasajero;
    }
    
    public Object[] toArray(){
        Object[] data = {idPasajero, nombrePasajero};
        return data;
    }
    
    
    
    
}
