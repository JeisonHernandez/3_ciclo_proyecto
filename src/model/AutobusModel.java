/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author jonco
 */
public class AutobusModel {
    private int idAutobus;
    private int asiento;

    public AutobusModel() {
    }
    
    public AutobusModel(int idAutobus, int asiento) {
        this.idAutobus = idAutobus;
        this.asiento = asiento;
    }

    /**
     * @return the idAutobus
     */
    public int getIdAutobus() {
        return idAutobus;
    }

    /**
     * @return the asiento
     */
    public int getAsiento() {
        return asiento;
    }

    /**
     * @param asiento the asiento to set
     */
    public void setAsiento(int asiento) {
        this.asiento = asiento;
    }
    
    
}
