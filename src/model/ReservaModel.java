/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author jonco
 */
public class ReservaModel {
    private int idReserva;
    private int idPasajero;
    private int idViaje;
    private boolean llegoEquipaje;

    public ReservaModel() {
    }

    public ReservaModel(int idReserva, int idPasajero, int idViaje, boolean llegoEquipaje) {
        this.idReserva = idReserva;
        this.idPasajero = idPasajero;
        this.idViaje = idViaje;
        this.llegoEquipaje = llegoEquipaje;
    }

    /**
     * @return the idReserva
     */
    public int getIdReserva() {
        return idReserva;
    }

    /**
     * @return the idPasajero
     */
    public int getIdPasajero() {
        return idPasajero;
    }

    /**
     * @param idPasajero the idPasajero to set
     */
    public void setIdPasajero(int idPasajero) {
        this.idPasajero = idPasajero;
    }

    /**
     * @return the idViaje
     */
    public int getIdViaje() {
        return idViaje;
    }

    /**
     * @param idViaje the idViaje to set
     */
    public void setIdViaje(int idViaje) {
        this.idViaje = idViaje;
    }

    /**
     * @return the llegoEquipaje
     */
    public boolean isLlegoEquipaje() {
        return llegoEquipaje;
    }

    /**
     * @param llegoEquipaje the llegoEquipaje to set
     */
    public void setLlegoEquipaje(boolean llegoEquipaje) {
        this.llegoEquipaje = llegoEquipaje;
    }
    
    
    
}
