/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;

import acces.AutobusDAO;
import acces.CiudadDAO;
import acces.PasajeroDAO;

import model.EmpleadoModel;
import model.CiudadModel;
import model.AutobusModel;
import model.PasajeroModel;

/**
 *
 * @author casierrav
 */
public class InfoInicial {
    private ArrayList<AutobusModel>       buses       = null;
    private ArrayList<CiudadModel>        ciudades   = null;
    private ArrayList<EmpleadoModel>      empleados = null;
    private ArrayList<PasajeroModel>      pasajeros = null;
    
    /**
     * Zero-parameters constructor
     */
    public InfoInicial(){
        CiudadDAO ciudadDao = new CiudadDAO();
        this.ciudades = ciudadDao.getAllCiudades();
        //this.ciudades.add(0, new CiudadModel("000", "Todas las ciudades"));
        
        PasajeroDAO pasajerosDao = new PasajeroDAO();
        this.pasajeros = pasajerosDao.getAllPasajeros();
        //this.pasajeros.add(0, new PasajeroModel(-1, "Todos los pasajeros"));
        
    }

    /**
     * @return the museums
     */
    public ArrayList<CiudadModel> getCiudades() {
        return ciudades;
    }

    /**
     * @return the exhibitions
     */
    public ArrayList<PasajeroModel> getPasajeros() {
        return pasajeros;
    }

}