/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import view.MainView;
import view.ResultadosPanel;

/**
 *
 * @author jonco
 */
public class ListenerCiudad implements ActionListener{
    
    //private MainView mainView;
    private ResultadosPanel resultadosPanel;

    public ListenerCiudad(ResultadosPanel resultadosPanel) {
        this.resultadosPanel = resultadosPanel;
    }
    
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        
        InfoInicial infoInicial = new InfoInicial();
        
        this.resultadosPanel.setTblResultsCiudades(infoInicial.getCiudades());
        
    }
    
}
