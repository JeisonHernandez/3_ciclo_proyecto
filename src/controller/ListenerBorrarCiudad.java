/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import acces.CiudadDAO;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import model.CiudadModel;
import view.AgregarCiudad;
import view.MainView;
import view.ResultadosPanel;

/**
 *
 * @author jonco
 */
public class ListenerBorrarCiudad implements ActionListener{
    
    private AgregarCiudad agregarCiudad;
    private ResultadosPanel resultadosPanel;

    public ListenerBorrarCiudad(ResultadosPanel resultadosPanel,AgregarCiudad agregarCiudad ) {
        this.resultadosPanel = resultadosPanel;
        this.agregarCiudad = agregarCiudad;
    }
    
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        
        CiudadDAO ciudadDao = new CiudadDAO();
        
        String idCiudadtablaSelected = this.resultadosPanel.getIdTblSelected();
        
        String idCiudad = this.agregarCiudad.getIdCiudad().getText();
        
        if (!idCiudad.isEmpty()){
            
            ciudadDao.deleteCiudad(idCiudad);  
            
        }else{
            if("-2".equals(idCiudadtablaSelected)){
                
                System.out.println("hola");
                JOptionPane.showMessageDialog(null,"No ingresó Id");
            }
            else{
                idCiudad = idCiudadtablaSelected;
                ciudadDao.deleteCiudad(idCiudad);
            }
            
        }
                     
        this.agregarCiudad.setlimpios();
        
        InfoInicial infoInicial = new InfoInicial();
        this.resultadosPanel.setTblResultsCiudades(infoInicial.getCiudades());
        
    }
    
}
