/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import acces.CiudadDAO;
import acces.PasajeroDAO;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import model.CiudadModel;
import model.PasajeroModel;
import view.AgregarCiudad;
import view.AgregarPersona;
import view.MainView;
import view.ResultadosPanel;

/**
 *
 * @author jonco
 */
public class ListenerCrearPasajero implements ActionListener{
    
    private AgregarPersona agregarPersona;
    private ResultadosPanel resultadosPanel;

    public ListenerCrearPasajero(ResultadosPanel resultadosPanel,AgregarPersona agregarPersona ) {
        this.resultadosPanel = resultadosPanel;
        this.agregarPersona = agregarPersona;
    }
    
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        
        PasajeroDAO pasajeroDao = new PasajeroDAO();
        
        String nombrePasajero = this.agregarPersona.getNombrePasajero();
        
        PasajeroModel pasajero = new PasajeroModel(nombrePasajero);
        pasajeroDao.insertPasajero(pasajero);
        
        this.agregarPersona.setLimpios();
        
        InfoInicial infoInicial = new InfoInicial();
        
        this.resultadosPanel.setTblResultsPasajeros(infoInicial.getPasajeros());
        
        
    }
    
}
