/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import acces.CiudadDAO;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import model.CiudadModel;
import view.AgregarCiudad;
import view.MainView;
import view.ResultadosPanel;

/**
 *
 * @author jonco
 */
public class ListenerCrearCiudad implements ActionListener{
    
    private AgregarCiudad agregarCiudad;
    private ResultadosPanel resultadosPanel;

    public ListenerCrearCiudad(ResultadosPanel resultadosPanel,AgregarCiudad agregarCiudad ) {
        this.resultadosPanel = resultadosPanel;
        this.agregarCiudad = agregarCiudad;
    }
    
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        
        CiudadDAO ciudadDao = new CiudadDAO();
        
        String idCiudad = this.agregarCiudad.getIdCiudad().getText();
        String nombreCiudad = this.agregarCiudad.getNombreCiudad().getText();
        
        CiudadModel ciudad = new CiudadModel(idCiudad, nombreCiudad);
        ciudadDao.insertCiudad(ciudad);
        
        this.agregarCiudad.setlimpios();

        InfoInicial infoInicial = new InfoInicial();
        
        this.resultadosPanel.setTblResultsCiudades(infoInicial.getCiudades());
        
    }
    
}
