/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import acces.CiudadDAO;
import acces.PasajeroDAO;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import model.CiudadModel;
import view.AgregarCiudad;
import view.AgregarPersona;
import view.MainView;
import view.ResultadosPanel;

/**
 *
 * @author jonco
 */
public class ListenerBorrarPasajero implements ActionListener{
    
    private AgregarPersona agregarPersona;
    private ResultadosPanel resultadosPanel;

    public ListenerBorrarPasajero(ResultadosPanel resultadosPanel,AgregarPersona agregarPersona ) {
        this.resultadosPanel = resultadosPanel;
        this.agregarPersona = agregarPersona;
    }
    
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        
        PasajeroDAO pasajeroDao = new PasajeroDAO();
        
        int idPasajeroSelected = Integer.parseInt(this.resultadosPanel.getIdTblSelected());
        
        int idPasajero = this.agregarPersona.getIdPasajero();

        
        if (!(idPasajero == -2) ){
            
           pasajeroDao.deletePasajero(idPasajero);   
           
        }else{
            if(idPasajeroSelected == -2){
                
                System.out.println("hola");
                JOptionPane.showMessageDialog(null,"No ingresó Id");
            }else{
                idPasajero = idPasajeroSelected;
                pasajeroDao.deletePasajero(idPasajero);
            }
            
            
        }
        
        this.agregarPersona.setLimpios();
        
        InfoInicial infoInicial = new InfoInicial();
        this.resultadosPanel.setTblResultsPasajeros(infoInicial.getPasajeros());
        
    }
    
}
