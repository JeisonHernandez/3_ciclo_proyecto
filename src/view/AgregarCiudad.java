/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import RSMaterialComponent.RSTextFieldIconTwo;
import controller.ListenerBorrarCiudad;
import controller.ListenerBuscarCiudad;
import controller.ListenerCrearCiudad;
import controller.ListenerModificarCiudad;

/**
 *
 * @author jonco
 */
public class AgregarCiudad extends javax.swing.JPanel {

    /**
     * Creates new form AgregarCiudad
     */
    ResultadosPanel rp = null;
    
    public AgregarCiudad(ResultadosPanel rp) {
        this.rp = rp;
        initComponents();
        
        ListenerCrearCiudad listenerCrearCiudad = new ListenerCrearCiudad(this.rp,this);
        crearCiudadBTN.addActionListener(listenerCrearCiudad);
        
        ListenerBuscarCiudad listenerBuscarCiudad = new ListenerBuscarCiudad(this.rp,this);
        buscarCiudadBTN.addActionListener(listenerBuscarCiudad);
        
        ListenerBorrarCiudad listenerBorrarCiudad = new ListenerBorrarCiudad(this.rp,this);
        eliminarCiudadBTN.addActionListener(listenerBorrarCiudad);
        
        ListenerModificarCiudad listenerModificarCiudad = new ListenerModificarCiudad(this.rp,this);
        actualizarCiudadBTN.addActionListener(listenerModificarCiudad);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        IdTFJ = new RSMaterialComponent.RSTextFieldIconTwo();
        ciduadTFJ = new RSMaterialComponent.RSTextFieldIconTwo();
        crearCiudadBTN = new RSMaterialComponent.RSButtonMaterialIconOne();
        buscarCiudadBTN = new RSMaterialComponent.RSButtonMaterialIconOne();
        actualizarCiudadBTN = new RSMaterialComponent.RSButtonMaterialIconOne();
        eliminarCiudadBTN = new RSMaterialComponent.RSButtonMaterialIconOne();

        IdTFJ.setForeground(new java.awt.Color(0, 204, 204));
        IdTFJ.setBorderColor(new java.awt.Color(0, 204, 204));
        IdTFJ.setColorIcon(new java.awt.Color(0, 204, 204));
        IdTFJ.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.GRAIN);
        IdTFJ.setPlaceholder("Id");

        ciduadTFJ.setForeground(new java.awt.Color(0, 204, 204));
        ciduadTFJ.setBorderColor(new java.awt.Color(0, 204, 204));
        ciduadTFJ.setColorIcon(new java.awt.Color(0, 204, 204));
        ciduadTFJ.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.LOCATION_CITY);
        ciduadTFJ.setPlaceholder("Ciudad");

        crearCiudadBTN.setBackground(new java.awt.Color(0, 153, 153));
        crearCiudadBTN.setText("Crear");
        crearCiudadBTN.setBackgroundHover(new java.awt.Color(0, 204, 204));
        crearCiudadBTN.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.ADD);

        buscarCiudadBTN.setBackground(new java.awt.Color(0, 153, 153));
        buscarCiudadBTN.setText("Buscar");
        buscarCiudadBTN.setBackgroundHover(new java.awt.Color(0, 204, 204));
        buscarCiudadBTN.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.SEARCH);

        actualizarCiudadBTN.setBackground(new java.awt.Color(0, 153, 153));
        actualizarCiudadBTN.setText("Actualizar");
        actualizarCiudadBTN.setBackgroundHover(new java.awt.Color(0, 204, 204));
        actualizarCiudadBTN.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CREATE);

        eliminarCiudadBTN.setBackground(new java.awt.Color(0, 153, 153));
        eliminarCiudadBTN.setText("Eliminar");
        eliminarCiudadBTN.setBackgroundHover(new java.awt.Color(0, 204, 204));
        eliminarCiudadBTN.setIcons(rojeru_san.efectos.ValoresEnum.ICONS.CLEAR);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(35, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(ciduadTFJ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(IdTFJ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(crearCiudadBTN, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 109, Short.MAX_VALUE)
                        .addComponent(buscarCiudadBTN, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(eliminarCiudadBTN, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 109, Short.MAX_VALUE)
                        .addComponent(actualizarCiudadBTN, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                .addGap(22, 22, 22))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(IdTFJ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(ciduadTFJ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(crearCiudadBTN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buscarCiudadBTN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(actualizarCiudadBTN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(eliminarCiudadBTN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(33, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    public RSTextFieldIconTwo getIdCiudad(){
        return IdTFJ;
    }
    
    public RSTextFieldIconTwo getNombreCiudad(){
        return ciduadTFJ;
    }
    
    public void setlimpios(){
        IdTFJ.setText("");
        ciduadTFJ.setText("");
    }
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private RSMaterialComponent.RSTextFieldIconTwo IdTFJ;
    private RSMaterialComponent.RSButtonMaterialIconOne actualizarCiudadBTN;
    private RSMaterialComponent.RSButtonMaterialIconOne buscarCiudadBTN;
    private RSMaterialComponent.RSTextFieldIconTwo ciduadTFJ;
    private RSMaterialComponent.RSButtonMaterialIconOne crearCiudadBTN;
    private RSMaterialComponent.RSButtonMaterialIconOne eliminarCiudadBTN;
    // End of variables declaration//GEN-END:variables
}
