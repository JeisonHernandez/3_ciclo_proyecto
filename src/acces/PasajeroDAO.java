/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acces;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

import java.util.ArrayList;
import model.CiudadModel;
import model.PasajeroModel;
import utils.ConnectionDB;

/**
 *
 * @author jonco
 */
public class PasajeroDAO {
    
    /**
     * Verificar Conección a la base de datos
     */
    private Connection conn = null;
    
    public ArrayList<PasajeroModel> getAllPasajeros() {
        ArrayList<PasajeroModel> pasajeros = new ArrayList();

            try {
                if(conn == null)//Verifica la conección
                    conn = ConnectionDB.getConnection();

                String sql = "SELECT idPasajero, nombrePasajero FROM Pasajero;";//Query de los datos que se quiere mostrar

                Statement statement = conn.createStatement();
                ResultSet result    = statement.executeQuery(sql);//Ejecuta el Query

                while (result.next()) {//Guarda los datos obtenidos de la base de datos en una lista de tipo Pasajero
                    PasajeroModel pasajero = new PasajeroModel(result.getInt(1), result.getString(2));
                    pasajeros.add( pasajero );
                }
            } 
            catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Código : " + ex.getErrorCode() 
                                            + "\nError :" + ex.getMessage());
            }
            return pasajeros;
    }
    
    public ArrayList<PasajeroModel> getNombrePasajeroPorID(int id) {
        
        PasajeroModel pasajero = new PasajeroModel();
        ArrayList<PasajeroModel> listaPasajero = new ArrayList<>();
        
        try {
            if(conn == null)//Verificar conección
                conn = ConnectionDB.getConnection();
            
            String sql = "SELECT nombrePasajero FROM Pasajero WHERE idPasajero=?;";//Query para obtener el numero de asientos del bus
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, id);//sustituir el id del bus en el Query
            
            ResultSet result = statement.executeQuery();//Ejecutar el Query
            
            while (result.next()) {//Guarda la informacion de la base de datos en un objeto de tipo Autobus
                pasajero.setIdPasajero(id);
                pasajero.setNombrePasajero(result.getString(1));
                break;
            }
        } 
        catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Código : " + ex.getErrorCode() 
                                        + "\nError :" + ex.getMessage());
        }
        listaPasajero.add(pasajero);
        
        return listaPasajero;
    }
    
    public ArrayList<PasajeroModel> getNombrePasajeroPorNombre(String nombre) {
        
        
        ArrayList<PasajeroModel> listaPasajero = new ArrayList<>();
        
        try {
            if(conn == null)//Verificar conección
                conn = ConnectionDB.getConnection();
            
            String sql = "SELECT idPasajero, nombrePasajero FROM Pasajero WHERE nombrePasajero LIKE '%" + nombre + "%';";//Query para obtener el numero de asientos del bus
            PreparedStatement statement = conn.prepareStatement(sql);
            //statement.setInt(1, id);//sustituir el id del bus en el Query
            
            ResultSet result = statement.executeQuery();//Ejecutar el Query
            
            while (result.next()) {//Guarda la informacion de la base de datos en un objeto de tipo Autobus
                PasajeroModel pasajero = new PasajeroModel(result.getInt(1), result.getString(2));
                listaPasajero.add(pasajero);
         
            }
        } 
        catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Código : " + ex.getErrorCode() 
                                        + "\nError :" + ex.getMessage());
        }
        
        
        return listaPasajero;
    }
    
    public void insertPasajero(PasajeroModel pasajero){
        try {
            if(conn == null)//Verifica la conección
                conn = ConnectionDB.getConnection();
            
            String sql = "INSERT INTO Pasajero(nombrePasajero) VALUES (?);";//Query del registro
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, pasajero.getNombrePasajero());//Cambia el valor del número de asientos en el query del bus a registrar
            
            int rowsInserted = statement.executeUpdate();//Ejecuta la Query-retorna numero de filas modificadas
            
            if(rowsInserted > 0) // verificar cuantas filas se modificaron
                JOptionPane.showMessageDialog(null, "El registro fue agregado exitosamente !");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Código : " + ex.getErrorCode() 
                                        + "\nError :" + ex.getMessage());
        }
    }
    
    
    public void updatePasajero(PasajeroModel pasajero) {
        try {
            if(conn == null)//Verificar coneccion
                conn = ConnectionDB.getConnection();
            
            String sql = "UPDATE Pasajero SET nombrePasajero =? WHERE idPasajero=?;";//Query para realizar la actualizacion
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, pasajero.getNombrePasajero());//Cambiar los datos a modificar en la BD
            statement.setInt(2, pasajero.getIdPasajero());// Registro a modificar
            
            int rowsUpdated = statement.executeUpdate();//Ejecutar Query-Retorna numero de filas modificadas
            
            if (rowsUpdated > 0) //Verificar filas modificadas
                JOptionPane.showMessageDialog(null, "El registro fue actualizado exitosamente !");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Código : " + ex.getErrorCode() 
                                        + "\nError :" + ex.getMessage());
        }
    }
    
    
    public void deletePasajero(int id) {
        try {
            if(conn == null)//Verificar coneccion
                conn = ConnectionDB.getConnection();
            
            String sql = "DELETE FROM Pasajero WHERE idPasajero=?;";//Query para eliminar un registro
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, id);
            
            int rowsDeleted = statement.executeUpdate();//Borarr registro
            
            if (rowsDeleted > 0) {//Verificar si la fila se elimino
                JOptionPane.showMessageDialog(null, "El registro fue borrado exitosamente !");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Código : "
                    + ex.getErrorCode() + "\nError :" + ex.getMessage());
        }
    }
    
}
