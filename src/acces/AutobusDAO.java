/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acces;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

import java.util.ArrayList;
import model.AutobusModel;
import utils.ConnectionDB;
/**
 *
 * @author jonco
 */
public class AutobusDAO {
    
    /**
     * Verificar Conección a la base de datos
     */
    private Connection conn = null;
    
    /**
     * Método que retornar todos los Autobuses registrados en la base de datos
     * @return lista de Autobuses
     */
    public ArrayList<AutobusModel> getAllAutobus() {
        ArrayList<AutobusModel> buses = new ArrayList();

            try {
                if(conn == null)//Verifica la conección
                    conn = ConnectionDB.getConnection();

                String sql = "SELECT idAutobus, asiento FROM Autobus;";//Query de los datos que se quiere mostrar
                Statement statement = conn.createStatement();
                ResultSet result    = statement.executeQuery(sql);//Ejecuta el Query

                while (result.next()) {//Guarda los datos obtenidos de la base de datos en una lista de tipo Autobus
                    AutobusModel bus = new AutobusModel(result.getInt(1), result.getInt(2));
                    buses.add( bus );
                }
            } 
            catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Código : " + ex.getErrorCode() 
                                            + "\nError :" + ex.getMessage());
            }
            return buses;
    }
    
    /**
     * Método que retorna el número de asientos en un autobus
     * @param id del Autobus
     * @return entero (asientos del autobus)
     */
    public int getAsientosPorID(int id) {
        
        AutobusModel bus = null;
        
        try {
            if(conn == null)//Verificar conección
                conn = ConnectionDB.getConnection();
            
            String sql = "SELECT asiento FROM presentacion WHERE idAutobus=?;";//Query para obtener el numero de asientos del bus
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, id);//sustituir el id del bus en el Query
            
            ResultSet result = statement.executeQuery();//Ejecutar el Query
            
            while (result.next()) {//Guarda la informacion de la base de datos en un objeto de tipo Autobus
                bus = new AutobusModel(id, result.getInt(1));
                break;
            }
        } 
        catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Código : " + ex.getErrorCode() 
                                        + "\nError :" + ex.getMessage());
        }
        
        return bus.getAsiento();
    }
    
    /**
     * Método registra un nuevo autobus
     * @param bus de tipo Autobus
     */
    public void insertAutobus(AutobusModel bus){
        try {
            if(conn == null)//Verifica la conección
                conn = ConnectionDB.getConnection();
            
            String sql = "INSERT INTO Autobus(Asiento) VALUES (?);";//Query del registro
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, bus.getAsiento());//Cambia el valor del número de asientos en el query del bus a registrar
            
            int rowsInserted = statement.executeUpdate();//Ejecuta la Query-retorna numero de filas modificadas
            
            if(rowsInserted > 0) // verificar cuantas filas se modificaron
                JOptionPane.showMessageDialog(null, "El registro fue agregado exitosamente !");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Código : " + ex.getErrorCode() 
                                        + "\nError :" + ex.getMessage());
        }
    }
    
    /**
     * Método que modifica un registro de la base de datos
     * @param bus de tipo Autobus 
     */
    public void updateAutobus(AutobusModel bus) {
        try {
            if(conn == null)//Verificar coneccion
                conn = ConnectionDB.getConnection();
            
            String sql = "UPDATE Autobus SET Asiento =? WHERE idAutobus=?;";//Query para realizar la actualizacion
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, bus.getAsiento());//Cambiar los datos a modificar en la BD
            statement.setInt(2, bus.getIdAutobus());// Registro a modificar
            
            int rowsUpdated = statement.executeUpdate();//Ejecutar Query-Retorna numero de filas modificadas
            
            if (rowsUpdated > 0) //Verificar filas modificadas
                JOptionPane.showMessageDialog(null, "El registro fue actualizado exitosamente !");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Código : " + ex.getErrorCode() 
                                        + "\nError :" + ex.getMessage());
        }
    }
    
    /**
     * Método que borra un registro de BD
     * @param id int que especifica el bus a borrar
     */
    public void deleteAutobus(int id) {
        try {
            if(conn == null)//Verificar coneccion
                conn = ConnectionDB.getConnection();
            
            String sql = "DELETE FROM Autobus WHERE idAutobus=?;";//Query para eliminar un registro
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, id);
            
            int rowsDeleted = statement.executeUpdate();//Borarr registro
            
            if (rowsDeleted > 0) {//Verificar si la fila se elimino
                JOptionPane.showMessageDialog(null, "El registro fue borrado exitosamente !");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Código : "
                    + ex.getErrorCode() + "\nError :" + ex.getMessage());
        }
    }
    
}
