/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acces;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

import java.util.ArrayList;
import model.CiudadModel;
import utils.ConnectionDB;


/**
 *
 * @author jonco
 */
public class CiudadDAO {
    
    /**
     * Verificar Conección a la base de datos
     */
    private Connection conn = null;
    
    public ArrayList<CiudadModel> getAllCiudades() {
        ArrayList<CiudadModel> ciudades = new ArrayList();

            try {
                if(conn == null)//Verifica la conección
                    conn = ConnectionDB.getConnection();

                String sql = "SELECT idCiudad, nombreCiudad FROM Ciudad;";//Query de los datos que se quiere mostrar

                Statement statement = conn.createStatement();
                ResultSet result    = statement.executeQuery(sql);//Ejecuta el Query

                while (result.next()) {//Guarda los datos obtenidos de la base de datos en una lista de tipo ciudad
                    CiudadModel ciudad = new CiudadModel(result.getString(1), result.getString(2));
                    ciudades.add( ciudad );
                }
            } 
            catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Código : " + ex.getErrorCode() 
                                            + "\nError :" + ex.getMessage());
            }
            return ciudades;
    }
    
    public ArrayList<CiudadModel> getCiudadPorID(String id) {

        ArrayList<CiudadModel> listaCiudad = new ArrayList<>();
        
        try {
            if(conn == null)//Verificar conección
                conn = ConnectionDB.getConnection();
            
            String sql = "SELECT idCiudad, nombreCiudad FROM Ciudad WHERE idCiudad LIKE '%" + id +"%';";//Query para obtener el numero de asientos del bus
            PreparedStatement statement = conn.prepareStatement(sql);
            //statement.setString(1, id);//sustituir el id del bus en el Query
            
            ResultSet result = statement.executeQuery();//Ejecutar el Query            

            while (result.next()) {//Guarda la informacion de la base de datos en un objeto de tipo Ciudad
                CiudadModel ciudad = new CiudadModel(result.getString(1), result.getString(2));
                listaCiudad.add(ciudad);
            }
 
        } 
        catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Código : " + ex.getErrorCode() 
                                        + "\nError :" + ex.getMessage());
        }       
        
        return listaCiudad;
    }
    
    public void insertCiudad(CiudadModel ciudad){
        try {
            if(conn == null)//Verifica la conección
                conn = ConnectionDB.getConnection();
            
            String sql = "INSERT  INTO  Ciudad(idCiudad,nombreCiudad) VALUES(?,?);";//Query del registro
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, ciudad.getIdCiudad());
            statement.setString(2, ciudad.getNombreCiudad());//Cambia el valor del número de asientos en el query de la ciudad a registrar
            
            int rowsInserted = statement.executeUpdate();//Ejecuta la Query-retorna numero de filas modificadas
            
            if(rowsInserted > 0) // verificar cuantas filas se modificaron
                JOptionPane.showMessageDialog(null, "El registro fue agregado exitosamente !");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Código : " + ex.getErrorCode() 
                                        + "\nError :" + ex.getMessage());
        }
    }
    
    public void updateCiudad(CiudadModel ciudad) {
        try {
            if(conn == null)//Verificar coneccion
                conn = ConnectionDB.getConnection();
            
            String sql = "UPDATE Ciudad SET nombreCiudad =? WHERE idCiudad=?;";//Query para realizar la actualizacion
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, ciudad.getNombreCiudad());//Cambiar los datos a modificar en la BD
            statement.setString(2, ciudad.getIdCiudad());// Registro a modificar
            
            int rowsUpdated = statement.executeUpdate();//Ejecutar Query-Retorna numero de filas modificadas
            
            if (rowsUpdated > 0) //Verificar filas modificadas
                JOptionPane.showMessageDialog(null, "El registro fue actualizado exitosamente !");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Código : " + ex.getErrorCode() 
                                        + "\nError :" + ex.getMessage());
        }
    }
    
    public void deleteCiudad(String id) {
        try {
            if(conn == null)//Verificar coneccion
                conn = ConnectionDB.getConnection();
            
            String sql = "DELETE FROM Ciudad WHERE idCiudad=?;";//Query para eliminar un registro
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, id);
            
            int rowsDeleted = statement.executeUpdate();//Borarr registro
            
            if (rowsDeleted > 0) {//Verificar si la fila se elimino
                JOptionPane.showMessageDialog(null, "El registro fue borrado exitosamente !");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Código : "
                    + ex.getErrorCode() + "\nError :" + ex.getMessage());
        }
    }
    
}
